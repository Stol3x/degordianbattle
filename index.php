<?php

$start = explode(' ', microtime());
$start = $start[1] + $start[0];

function autoload($class)
{
    if(file_exists('src/'.$class.'.php')) {
        require_once('src/'.$class.'.php');
    } else {
        throw new Exception('Object does not exists.');
    }
}

autoload('Battle');

if(!isset($_GET['army1']) || !isset($_GET['army2'])){
    die("Please put some soldiers in the battle. FFS. Put 10, 20 or 200 soldiers in this battle. This is not one man army. And, send whole reservest to war. War will be never over.");
}

$army1 = (int)$_GET['army1']; // US
$army2 = (int)$_GET['army2']; // RU

$battle = new Battle($army1, $army2);

$end = explode(' ', microtime());
$end = $end[1] + $end[0];

echo $end - $start;

?>