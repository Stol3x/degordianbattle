<?php

class Army
{
    public function __construct($soldiers, $side)
    {
        $this->num = $soldiers;
        $this->side = $side;
        return $this;
    }
    
    
    /**
     *     Select department for the soldier.
     * 
     *     Due the budget cuts, soldier has his right to choose his department. So, it's random.
     * 
     * @return String 
     */
    protected function selectDepartment()
    {
        $department = rand(0,100);
        
        if($department <= 5) {
            return 'Aircraft';
        } elseif ($department > 5 && $department < 30) {
            return 'Army';
        } else {
            return 'Marines';
        }
    }

    /**
     *     Arranges soldiers to their army.
     * 
     *     @return array
     */
    protected function arrangeSoldiers()
    {
        $army = [];
        for($i = 0; $i < $this->num - 1; $i++) {
            $army[] = $this->selectDepartment();
        }
        
        return $army;
    }
    
    /**
     *     Builds platoons depending on soldier's department
     * 
     *     @return array
     */
    protected function getPlatoons()
    {        
        $departments = $this->arrangeSoldiers();
        
        $platoons = ['Aircraft' => 0, 'Army' => 0, 'Marines' => 0];
        
        foreach($departments as $dept) {
            $platoons[$dept] = (isset($platoons[$dept]) ? $platoons[$dept]+1 : 1);
        }
        
        return $platoons;
    }
    
    /**
     *     Build an army with platoons.
     * 
     *     Generating new platoon for the each pre-generated number of soldiers assigned to department.
     * 
     *     @return array
     */
    public function getArmy()
    {
        autoload('Platoon');
        
        $army = [];
        $this->platoons = $this->getPlatoons();

        foreach($this->platoons as $dept => $platoon) {
            $army[$dept] = (new Platoon($platoon, $this->side, $dept))->getPlatoon();
            if(empty($army[$dept])) {
                $army[$dept] = [['squad' => ['classified' => ['noWeapon' => []]]]];
            }
        }
        
        return $army;
    }
    
}

?>