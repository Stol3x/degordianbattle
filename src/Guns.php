<?php

class Guns
{
    public function __construct()
    {
        return $this;
    }
    
    /**
     *     Get rifles specific for each side
     * 
     *     Every gun, rifle or vehicle has specific data, such as damage, speed of the bullet etc.
     *     Just bunch of number, but, very important for battle and course of the battle.
     *     Squads are arranged by their class while army has their vehicles as airforce
     * 
     *     This list is expandable, you can add various tanks, jets etc. A10 and frogfoot are here by default because they have great attack on tanks.
     * 
     *     @return array
     */
    public function getGuns()
    {
        $guns = [
            'Marines' => [
                'US' => [
                    'assault' => [
                        'm16' => [
                            'muzzleVelocity'     => 950,
                            'damage'             => 80,
                            'effRange'             => 800,
                            'mag'                 => 30,
                            'rateOfFire'         => 700,
                            'health'            => 100,
                            'speed'                => 1.3
                        ]
                    ],
                    'engineer' => [
                        'm4a1' => [
                            'muzzleVelocity'     => 880,
                            'damage'            => 40,
                            'effRange'             => 500,
                            'mag'                 => 30,
                            'rateOfFire'         => 700,
                            'health'            => 100,
                            'speed'                => 1.3
                        ]
                    ],
                    'support' => [
                        'm249' => [
                            'muzzleVelocity'     => 915,
                            'damage'            => 80,
                            'effRange'             => 700,
                            'mag'                 => 100,
                            'rateOfFire'         => 800,
                            'health'            => 100,
                            'speed'                => 0.8
                        ]
                    ],
                    'recon' => [
                        'm99' => [
                            'muzzleVelocity'     => 980,
                            'damage'            => 100,
                            'effRange'             => 2600,
                            'mag'                 => 10,
                            'rateOfFire'         => 1,
                            'health'            => 100,
                            'speed'                => 0.8
                        ]
                    ]
                ],
                'RU' => [
                    'assault' => [
                        'AK74' => [
                            'muzzleVelocity'     => 625,
                            'damage'             => 60,
                            'effRange'             => 3000,
                            'mag'                 => 20,
                            'rateOfFire'         => 650,
                            'health'            => 100,
                            'speed'                => 1.3
                        ]
                    ],
                    'engineer' => [
                        'AKM' => [
                            'muzzleVelocity'     => 715,
                            'damage'             => 40,
                            'effRange'             => 400,
                            'mag'                 => 20,
                            'rateOfFire'         => 600,
                            'health'            => 100,
                            'speed'                => 1.3
                        ]
                    ],
                    'support' => [
                        'PKM' => [
                            'muzzleVelocity'     => 825,
                            'damage'            => 90,
                            'effRange'             => 1500,
                            'mag'                 => 100,
                            'rateOfFire'         => 650,
                            'health'            => 100,
                            'speed'                => 0.8
                        ]
                    ],
                    'recon' => [
                        'OSV96' => [
                            'muzzleVelocity'     => 770,
                            'damage'             => 100,
                            'effRange'             => 2000,
                            'mag'                 => 5,
                            'rateOfFire'         => 1,
                            'health'            => 100,
                            'speed'                => 0.8
                        ]
                    ]
                ]
            ],
            'Aircraft' => [
                'US' => [
                    'jet' => [
                        'a10' => [
                            'ammo'         => 1200,
                            'damage'     => 1000,
                            'speed'        => 195,
                            'rateOfFire'=> 4200,
                            'health'    => 1000
                        ]
                    ]
                ],
                'RU' => [
                    'jet' => [
                        'frogfoot' => [
                            'ammo'         => 1200,
                            'damage'     => 1000,
                            'speed'        => 270,
                            'rateOfFire'=> 3000,
                            'health'    => 1000
                        ]
                    ]
                ]
            ],
            'Army' => [
                'US' => [
                    'tank' => [
                        'M1A1' => [
                            'ammo'         => 20,
                            'damage'    => 500,
                            'speed'        => 19,
                            'rateOfFire'=> 1,
                            'health'    => 10000
                        ]
                    ]
                ],
                'RU' => [
                    'tank' => [
                        'T90' => [
                            'ammo'         => 20,
                            'damage'    => 500,
                            'speed'        => 19,
                            'rateOfFire'=> 1,
                            'health'    => 10000
                        ]
                    ]
                ]
            ]
        
        ];

        return $guns;
    }

}

?>