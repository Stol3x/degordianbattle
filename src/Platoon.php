<?php

class Platoon
{
    public function __construct($number, $side, $dept)
    {
        $this->numOfSoldiers = $number;
        $this->side = $side;
        $this->dept = $dept;
        
        return $this;
    }
    
    /**
     *     Builds a platoon with squads.
     * 
     *     Each squad has 4 members. And platoon is an army made of all squads that are in same department.
     *     Budget cuts, remember, you can't send whole army to one battle.
     * 
     *     @return array
     */
    protected function buildPlatoon()
    {
        autoload('Squad');
        $squads = ceil($this->numOfSoldiers / 4);
        $sum = $squads * 4;
        $difference = $sum - $this->numOfSoldiers;
        $lastSquad = 4 - $difference;
        
        if($squads != 0) {
            for($i = 0; $i < $squads; $i++) {
                $platoon[] = (new Squad($this->side, $this->dept))->getSquad(($squads == $i) ? $lastSquad : 4);
            }
            
            return $platoon;
        }
    }
    
    /**
     *     Returns platoon.
     * 
     *     @return array
     */
    public function getPlatoon()
    {
        return $this->buildPlatoon();
    }
}

?>