<?php

class Squad
{
    public function __construct($side, $dept)
    {
        $this->side = $side;
        $this->dept = $dept;
        
        return $this;
    }
    
    /**
     *     Squad is team of 4 soldiers with different classes for different type of attack:
     *     Driver (recon), radioman (engineer), gunner (support) and assault and they walk, vehicles are too expensive
     * 
     *     @return array
     */
    protected function buildSquad($members)
    {
        autoload('Guns');
        $guns = (new Guns())->getGuns();
        
        $squad = [
            'squad' => $guns[$this->dept][$this->side],
        ];
        
        $i = 0;

        array_splice($squad['squad'], $members);
        
        /*if(empty($squad['squad'])) {
            $squad['squad'] = ['unclass' => []];
        }*/
        
        return $squad;
    }
    
    /**
     *     Returns an array of soldiers with their weapons
     * 
     *     @return array
     */
    public function getSquad($members)
    {
        return $this->buildSquad($members);
    }
}

?>