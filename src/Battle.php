<?php

class Battle
{
    
    /**
     *     Side which performs next attack.
     * 
     *     @var string
     */
    private $turn = 'US';
    
    /**
     *     Checking if side put their heavy weaponry into the game.
     * 
     *     @var array
     */
    private $heavy = [
        'US' => [
            'army' => false,
            'aircraft' => false
        ],
        'RU' => [
            'army' => false,
            'aircraft' => false
        ]
    ];
    
    /**
     *     Starting the battle. Setting up data for battle.
     *     
     *     Here is the logic:
     *     Put marines in, I know, this is Russia vs. USA, but, I'm not fan of communist armies and don't know their name.
     *     Put marines in, if attack is strong and you're left on only 30% of your infantry, you need help.
     *  Bring the army, with tanks and stuff. They will be helpful. Maybe. If your enemy still didn't put their tanks into the game, they need help.
     *     They will put their aircraft into game. BRRRRT. BRRRTs, freedom is here. 30mm bullets flies to your tanks. Good luck with that.
     *     By the logic of this game, probably ~80% of your tanks will be destroyed. And they still have their tanks alive.
     *     Battle is over when all infantry of one team is dead. Tanks always can return to the home, as aircraft too.
     */
    public function __construct($army1, $army2)
    {    
        $this->army1 = $army1;
        $this->army2 = $army2;
        
        autoload('Army');
        $this->us = (new Army($this->army1, 'US'))->getArmy();
        $this->ru = (new Army($this->army2, 'RU'))->getArmy();
        
        // Printing stats about arms
        $this->echoStats();
        
        $this->battle();
        
        return $this;
    }
    
    /**
     *     Printing stats about each army.
     * 
     *     Stuff like counting soldiers etc.
     */
    public function echoStats()
    {
        $usMarines = 0;
        foreach($this->us['Marines'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $usMarines++;
                }
            }
        }
        
        $usArmy = 0;
        foreach($this->us['Army'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $usArmy++;
                }
            }
        }
        
        $usAircraft = 0;
        foreach($this->us['Aircraft'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $usAircraft++;
                }
            }
        }
        
        // Counting russian team
        $ruMarines = 0;
        foreach($this->ru['Marines'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $ruMarines++;
                }
            }
        }
        
        $ruArmy = 0;
        foreach($this->ru['Army'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $ruArmy++;
                }
            }
        }
        
        $ruAircraft = 0;
        foreach($this->ru['Aircraft'] as $army) {
            foreach($army as $platoon) {
                foreach($platoon as $squad) {
                    $ruAircraft++;
                }
            }
        }
        
        echo 'Here are the statistics:<br />';
        echo 'US has: <b>'.$usMarines.'</b> Marines<br />';
        echo 'US has: <b>'.$usArmy.'</b> Tanks<br />';
        echo 'US has: <b>'.$usAircraft.'</b> Jets<br /><br />';
        
        echo 'RU has: <b>'.$ruMarines.'</b> Marines<br />';
        echo 'RU has: <b>'.$ruArmy.'</b> Tanks<br />';
        echo 'RU has: <b>'.$ruAircraft.'</b> Jets<br /><br /><br />';
    }
    
    /**
     *     You never don't know where your battle will be. So, better be prepared for everything.
     * 
     *     @return array
     */
    protected function getBattlefield()
    {
        /*
         * Each field has specific data. So, in desert you can't run in sand while your boots are full of fckn sand.
         * Wind in open areas as desert sometimes is strong. Wind could impact your bullet and precision.
         * 
         * Having battle in urban field at distance of 2000m is no-sense. You could destroy the whole city to find somebody.
         * But at other side, in urban areas your mobility is decreased because ruins, burning vehicles etc.
         * 
         * Meanwhile in forest, you're under heavy fire. Your enemy is behind next tree. Your teammate is suppressing above your head. You're deaf and can't hear shi*.
         * But, failure is not an option. Fight, run through woods, maybe hit some, but, fuc* it. Go hard or go home. Jk, you're dead.
         */
        $fields = [
            'desert' => [
                'wind'         => 10,
                'mobility'    => 0.9,
                'distance'    => 2000
            ],
            'urban' => [
                'wind'        => 1,
                'mobility'    => 0.1,
                'distance'    => 500
            ],
            'forest' => [
                'wind'        => 1,
                'mobility'    => 0.05,
                'distance'    => 200
            ]
        ];
        
        $field = array_rand($fields);
        $battlefield = $fields[$field];
        $battlefield['field'] = $field;
        
        return $battlefield;
    }

    /**
     *     Because there are different battlefields and different start points we have to put distance between soldiers.
     *     We will put their distance from the center of battlefield since they are walking to each other.
     */
    protected function setDistances()
    {
        foreach($this->us as $dept => $department) {
            foreach($department as $ptNum => $platoon) {
                foreach($platoon as $sqNum => $squad) {
                    foreach($squad as $class => $soldier) {
                        foreach($soldier as $weapon => $data) {
                            //die(print_r($this->us[$dept][$ptNum][$sqNum][$class][$weapon]));
                            if(!empty($this->us[$dept][$ptNum][$sqNum][$class][$weapon])) {
                                $this->us[$dept][$ptNum][$sqNum][$class][$weapon]['distance'] = $this->battlefield['distance'] / 2;
                            }
                        }
                    }
                }
            }
        }
        
        // We have to do same for the RU army.
        foreach($this->ru as $dept => $department) {
            foreach($department as $ptNum => $platoon) {
                foreach($platoon as $sqNum => $squad) {
                    foreach($squad as $class => $soldier) {
                        foreach($soldier as $weapon => $data) {
                            if(!empty($this->ru[$dept][$ptNum][$sqNum][$class][$weapon])) {
                                $this->ru[$dept][$ptNum][$sqNum][$class][$weapon]['distance'] = $this->battlefield['distance'] / 2;
                            }
                        }
                    }
                }
            }
        }

    }
    
    /**
     *     Operates with battle.
     */
    public function battle()
    {
        $this->battlefield = $this->getBattlefield();
        
        echo 'Battle started. Armies are ready. You\'re fighiting in '.$this->battlefield['field'].'<br />';
        //$this->battleStatus();
        $this->setDistances();
        $this->usInfantry = $this->countSoldiers('US');
        $this->ruInfantry = $this->countSoldiers('RU');
        
        
        
        while($this->battleStatus()) {
            $this->attack();
        }
    }
    
    /**
     *     Atacking soldiers of another team. Logic of attacks.
     */
    protected function attack()
    {
        if($this->percentOfAliveSoldiers($this->turn) <= 30) {
            $this->attackInfantry($this->turn);
            $this->insertArmy($this->turn);
        } else {
            $this->attackInfantry($this->turn);
        }
        
        // Switch sides.
        if($this->turn == 'US') {
            $this->turn = 'RU';
        } else {
            $this->turn = 'US';
        }
    }
    
    /**
     *     Inserting aircraft and attacking enemy tanks.
     */
    protected function insertAircraft($team)
    {
        if($this->heavy[$team]['aircraft'] === false) {
            echo '<b>'.$team.' Is inserting an aircraft</b><br /><br />';
            $this->aircraftAttack($team);
            $this->heavy[$team]['aircraft'] = true;
        }
    }
    
    /**
     *     Destroying enemy tanks. 
     * 
     *     Tanks have heavy armor so it's pretty hard to destroy them.
     *     Each plane have 10 burst fires available. Each burst fire takes 1000 helath points. So, result is 0.
     *     Conslusion: each jet can destroy one tank. Light 'em up. BRRRRRRRRT
     */
    protected function aircraftAttack($team)
    {
        $team1 = strtolower($team);
        $team2 = ($team == 'US') ? 'ru' : 'us';
        
        foreach($this->{$team1}['Aircraft'] as $army) {
            foreach($army['squad'] as $class) {
                foreach($class as $soldier) {
                    
                    // Maybe rounds will miss the tank, maybe he is hidding behind the bushes
                    $infantry = $this->{$team2}['Army'];
                    $platoon = array_rand($infantry);
                    $squad = array_rand($infantry[$platoon]['squad']);
                    $weapon = array_rand($infantry[$platoon]['squad'][$squad]);
                    
                    // Light him up
                    $soldier2 = $this->{$team2}['Army'][$platoon]['squad'][$squad][$weapon]['health'] = 0;
                }
            }
        }
    }
    /**
     *     If army is under heavy attack, attack enemy with army. They have big ones... tanks...
     *     if tanks are already inserted into the game, you can't re-insert them again. Budget cuts. No money for fuel.
     *     Please, next time, invade some country with fuel :)
     */
    protected function insertArmy($team)
    {
        // What? Do you want put all your resources into one battle? F*** no.
        if($this->heavy[$team]['army'] === false && $this->heavy[$team]['aircraft'] === false) {
            echo '<b>'.$team.' inserted an army</b><br /><br />';
            $this->armyAttack($team);
            $this->heavy[$team]['army'] = true;
            
            // As we said. If you call the tanks in the game, they will call aircraft.
            $this->insertAircraft(($this->turn == 'US') ? 'RU' : 'US');
        }
    }
    
    /**
     *     Tanks are heavy armored vehicles with high explosive 80+mm shells. They are heavy for reloading and firing.
     *     Tanks have big effective range of fire, so we won't worry about moving them. We can use them as artillery.
     *     Each HE shell will kill 5 enemies.
     *     We can't air for only alive soldiers, where grenade hits, it hits.
     */
    protected function armyAttack($team)
    {
        $team1 = strtolower($team);
        $team2 = ($team == 'US') ? 'ru' : 'us';
        
        foreach($this->{$team1}['Army'] as $platoon1 => $army) {
            foreach($army['squad'] as $squadNum1 => $class) {
                foreach($class as $soldierNum1 => $soldier) {
                    
                    if($soldier['health'] > 0) {
                        for($i = 0; $i < 5; $i++) {
                            $infantry = $this->{$team2}['Marines'];
                            $platoon = array_rand($infantry);
                            $squad = array_rand($infantry[$platoon]['squad']);
                            $weapon = array_rand($infantry[$platoon]['squad'][$squad]);
                            
                            $soldier2 = $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon]['health'] = 0;
                        }
                    }
                }
            }
        }
    }
    
    /**
     *     Each member of the squad fires at the random soldier in the other team
     */
    protected function attackInfantry($team)
    {
        $team1 = strtolower($team);
        foreach($this->{$team1}['Marines'] as $platoon1 => $army) {
            foreach($army['squad'] as $squadNum1 => $class) {
                foreach($class as $soldierNum1 => $soldier) {
                    
                    /*
                     * Lock and load.
                     * Aim.
                     * Shoot.
                     */
                    
                    if(!empty($soldier)) {
                        // Selecting random soldier from other team
                        $team2 = ($team == 'US') ? 'ru' : 'us';
                        $infantry = $this->{$team2}['Marines'];
                        $platoon = array_rand($infantry);
                        $squad = array_rand($infantry[$platoon]['squad']);
                        $weapon = array_rand($infantry[$platoon]['squad'][$squad]);
                        
                        $soldier2 = $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon];
                        
                        // If soldier is dead, don't shoot him. Aim for another one.
                        // This may sometimes loop for a longer time, so while soldier is looking for the soldiers, he may get shot.
                        $try = 0;
                        while(!empty($soldier2) && $soldier2['health'] <= 0) {
                            $platoon = array_rand($infantry);
                            $squad = array_rand($infantry[$platoon]['squad']);
                            $weapon = array_rand($infantry[$platoon]['squad'][$squad]);
                            $soldier2 = $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon];
                            $try++;
                            
                            if($try > 10) {
                                return;
                            }
                        }

                        $range = $soldier['distance'] + $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon]['distance'];
                        
                        // Check if soldier is in range of your rifle. You can't fire 5.56 at 2000m.
                        if($soldier['effRange'] >= $range) {
                            $chance = ($soldier['mag'] + $this->battlefield['wind'] + $soldier['rateOfFire']) / 100;
                            $rand = rand(0, 10);
                            
                            // Because wind and big rate of fire and recoil there are less chances you will shoot him down.
                            if($chance < $rand) {
                                $newHealth = $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon]['health'] - $soldier['damage'];
                            } else {
                                $newHealth = $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon]['health'];
                            }
                            
                            // If you shot it, set new health
                            $this->{$team2}['Marines'][$platoon]['squad'][$squad][$weapon]['health'] = $newHealth;
                        }
                        
                        // Move your soldier, he can walk towards enemy
                        $newDistance = $this->{$team1}['Marines'][$platoon1]['squad'][$squadNum1][$soldierNum1]['distance'] - ($soldier['speed'] + $this->battlefield['mobility'])/10;
                        $this->{$team1}['Marines'][$platoon1]['squad'][$squadNum1][$soldierNum1]['distance'] = $newDistance;
                    }
                    
                }
            }
        }
    }
    
    /**
     *     Counting infantry at the start
     * 
     *     @return int
     */
    protected function countSoldiers($team)
    {
        $soldiers = 0;
        foreach($this->{strtolower($team)}['Marines'] as $army) {
            foreach($army['squad'] as $class) {
                foreach($class as $soldier) {                        
                    $soldiers++;
                }
            }
        }
        
        return $soldiers;
    }
    
    /**
     *     Counting alive soldiers in the team
     * 
     *     @return int
     */
    protected function countAliveSoldiers($team)
    {
        $alive = 0;
        
        foreach($this->{strtolower($team)}['Marines'] as $army) {
            foreach($army['squad'] as $class) {
                foreach($class as $soldier) {
                    if(!empty($soldier)) {
                        if($soldier['health'] > 0) {
                            $alive++;
                        }
                    }
                }
                
            }
        }
        
        return $alive;
    }
    
    /**
     *     Calculating percentage of alive infantry 
     */
    protected function percentOfAliveSoldiers($team)
    {
        return ($this->countAliveSoldiers($team) / $this->{strtolower($team).'Infantry'}) * 100;
    }
    
    /**
     *     Checking battle status. If there is alive soldiers in both teams game keeps going on.
     *     If one team has no more alive soldiers, other team won. Battle is over.
     * 
     *     @return bool
     */
    public function battleStatus()
    {    
        echo 'Soldiers alive:<br /> US: '.$this->countAliveSoldiers('US'). ' <br />RU: '.$this->countAliveSoldiers('RU').'<br /><br />';
                
        if($this->countAliveSoldiers('US') == 0) {
            echo 'US team is dead. RU team won. Better luck next time.';
            return false;
        } elseif ($this->countAliveSoldiers('RU') == 0) {
            echo 'RU team is dead. US team won. Better luck next time.';
            return false;
        } else {
            return true;
        }
    }
    
}

?>